# Countdown

Execute a command after a dramatic countdown.

Stdin is forwarded so countdown can be used in the middle of a
pipe-chain to indicate when a command is executed the first time.

The countdown is sent to stderr. This means that it is shown in the
terminal, but doesn't end up in stdin of the next command in the pipe.

## Examples

    countdown ls

    countdown 10 echo "Start rockets!"

    echo "cat\ntiger\ndog" | countdown 5 sort
